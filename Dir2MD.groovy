@Grapes(
    @Grab(group='org.apache.commons', module='commons-lang3', version='3.0')
)
import groovy.io.FileType;
import org.apache.commons.lang3.StringUtils;

def baseDirName = "/media/stuff/dev/repo/ebeosztas/ebeosztas/src"
def namePrefix = "src/"
def out = new File("/home/gyuuu/melleklet.md")
def heading1 = "Állományok"
def includedTypes = ["java", "xml", "jsp", "tag", "js","css","properties"]
def excludedTypes = ["min.js", "min.css"]
def baseDir = new File(baseDirName)
def lines = []
lines << heading1
lines << "===="
baseDir.eachFileRecurse(FileType.FILES) {
    f-> 
    def skip = true
    for (type in includedTypes) {
	if(f.name.endsWith(type)){ 
	    skip = false
	    break    
	}
    }
    for(type in excludedTypes){
	if(f.name.endsWith(type)){
	    skip = true
	    break
	}
    }
    if(!skip)
    {
    def name = namePrefix+f.canonicalPath.substring(baseDirName.size())
    lines << name
    lines << "----"
    f.eachLine("UTF-8", 0)  {fLine ->
	if(!StringUtils.isBlank(fLine))
		lines << "\t\t\t\t" + fLine
    }
    }
}
out.withWriter {w->
    for(def line:lines){
	w.writeLine(line)
    	println(line)
    }
}
